// Package bathingwateruk is a crawler for Thingful.
// It returns data about water quality at coastal sites in England and Wales
// collected under the terms of the European Bathing Water Directive
package bathingwateruk

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"sort"
	"strconv"
	"sync"
	"time"

	"github.com/thingful/thingfulx"
	"github.com/thingful/thingfulx/schema"
)

const (

	// indexerUID is the name of this indexer
	indexerUID = "bathingwateruk"

	// dataLicenseURL is the url of the datalicense
	dataLicenseURL = thingfulx.OGLV3URL

	// attributionName is the name of copyright holder and/or author of this data
	attributionName = "Environment Agency"

	// attributionURL is the url of copyright holder and/or author of this data
	attributionURL = "https://www.gov.uk/government/organisations/environment-agency"

	// provider is the upstream data provider
	provider = "environment.data.gov.uk"

	// providerWebsite is the upstream data provider website
	providerWebsite = "http://environment.data.gov.uk/"

	// englandURL is URL used to query water data in England
	englandURL = "http://environment.data.gov.uk/doc/bathing-water.json"

	// walesURL is the URL used to query water data in Wales
	walesURL = "http://environment.data.gov.uk/wales/bathing-waters/doc/bathing-water.json"

	// pagesize is the number of item sent as URL query to the provider data sets
	pageSize = "1000"
)

// BathingWaterFeeds struct is used to retrive a list of available
// water monitoring stations
type BathingWaterFeeds struct {
	Items item `json:"result"`
}

type item struct {
	StationData []stationURL `json:"items"`
}

type stationURL struct {
	URL string `json:"_about"`
}

// BathingWaterFeed struct is used to parse the latest water measurements
// collected from and individual monitoring station
type BathingWaterFeed struct {
	Result result `json:"result"`
}

type result struct {
	Topic topic `json:"primaryTopic"`
}

type topic struct {
	StationLocationName   areaName      `json:"bwq_bathingWater"`
	SamplingPointLocation samplingPoint `json:"bwq_samplingPoint"`
	ECCount               int           `json:"escherichiaColiCount"`
	ECQualifier           ecQualifier   `json:"escherichiaColiQualifier"`
	IEQualifier           ieQualifier   `json:"intestinalEnterococciQualifier"`
	IECount               int           `json:"intestinalEnterococciCount"`
	RecordedAt            recordedAt    `json:"sampleDateTime"`
}

type areaName struct {
	StationURI string        `json:"_about"`
	Name       areaNameValue `json:"name"`
}

type areaNameValue struct {
	Value string `json:"_value"`
}

type samplingPoint struct {
	Lat          float64          `json:"lat"`
	Lgn          float64          `json:"long"`
	SamplingName samplinPointName `json:"name"`
}

type samplinPointName struct {
	Value string `json:"_value"`
}

type ecQualifier struct {
	Notation string `json:"countQualifierNotation"`
}

type ieQualifier struct {
	Notation string `json:"countQualifierNotation"`
}

type recordedAt struct {
	Time dateTime `json:"inXSDDateTime"`
}

type dateTime struct {
	Value string `json:"_value"`
}

// StationFeed structs is used for retrieving latestAssasment URL
type StationFeed struct {
	Result stData `json:"result"`
}

type stData struct {
	Item assessment `json:"primaryTopic"`
}

type assessment struct {
	LatestAssessment string `json:"latestSampleAssessment"`
}

// NewIndexer instantiates a new Indexer instance.
func NewIndexer() (thingfulx.Indexer, error) {

	// provider object
	provider := &thingfulx.Provider{
		Name:        provider,
		ID:          indexerUID,
		URL:         providerWebsite,
		Description: "The environment.data.gov.uk website provides open data from the Department of Environment, Food and Rural Affairs",
	}

	return &indexer{
		provider: provider,
	}, nil
}

type indexer struct {
	provider *thingfulx.Provider
}

// UID returns unique identifier of this indexer
func (i *indexer) UID() string {
	return indexerUID
}

type SamplesResponse struct {
	URLs []string
	Err  error
}

// URLS returns the minimum set of urls required to fully index this data provider
func (i *indexer) URLS(ctx context.Context, client thingfulx.Client, delay time.Duration) ([]string, error) {
	samplesURLs := [2]string{englandURL, walesURL}
	var err error
	var samplesResponse SamplesResponse
	var wg sync.WaitGroup

	ch := make(chan SamplesResponse)

	for _, dataURL := range samplesURLs {
		wg.Add(1)

		// add limit argument to URLS
		u, err := url.Parse(dataURL)
		if err != nil {
			return nil, err
		}

		v := url.Values{}
		v.Set("_pageSize", pageSize)

		u.RawQuery = v.Encode()

		go func(u string) {
			defer wg.Done()

			ch <- getSamplesURLs(u, client)
		}(u.String())
	}

	// close channel after goroutines are finished
	go func() {
		wg.Wait()
		close(ch)
	}()

	for r := range ch {
		if r.Err != nil {
			err = r.Err
			continue
		}

		samplesResponse.URLs = append(samplesResponse.URLs, r.URLs...)
	}

	sort.Strings(samplesResponse.URLs)

	return samplesResponse.URLs, err
}

func getSamplesURLs(rawURL string, client thingfulx.Client) SamplesResponse {
	var waterData BathingWaterFeeds
	var urlsToFetch []string
	samplesResponse := SamplesResponse{}

	bytes, err := client.DoHTTPGetRequest(rawURL)
	if err != nil {
		samplesResponse.Err = err
		return samplesResponse
	}

	// remove query from url
	u, _ := url.Parse(rawURL)
	q := u.Query()
	q.Del("_pageSize")

	u.RawQuery = q.Encode()

	err = json.Unmarshal(bytes, &waterData)
	if err != nil {
		samplesResponse.Err = err
		return samplesResponse
	}

	for _, sampleURL := range waterData.Items.StationData {
		stationURL := sampleURL.URL
		urlsToFetch = append(urlsToFetch, fmt.Sprintf("%s.json", stationURL))
	}

	samplesResponse.URLs = urlsToFetch

	return samplesResponse
}

// Fetch a resource from the upstream provider if we can.
func (i *indexer) Fetch(ctx context.Context, url string, client thingfulx.Client) ([]byte, error) {
	var stationFeed StationFeed

	b, err := client.DoHTTPGetRequest(url)
	if err != nil {
		return nil, err
	}

	// parse the latest sample URL from he current station
	err = json.Unmarshal(b, &stationFeed)
	if err != nil {
		return nil, err
	}

	if stationFeed.Result.Item.LatestAssessment == "" {
		return nil, thingfulx.NewErrBadData("Could not find the latest sample URL")
	}

	latestSampleURL := stationFeed.Result.Item.LatestAssessment + ".json"

	b2, err := client.DoHTTPGetRequest(latestSampleURL)
	if err != nil {
		return nil, err
	}

	return b2, nil
}

/// Parse is our function to actually extract data and return the slice of things
func (i *indexer) Parse(rawData []byte, dataURL string, timeProvider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {
	things := []thingfulx.Thing{}
	waterData := BathingWaterFeed{}

	// parse incoming data
	err := json.Unmarshal(rawData, &waterData)

	if err != nil {
		return nil, err
	}

	// build thing
	thing, err := i.buildThing(waterData, dataURL, timeProvider, rawData)
	if err != nil {
		return nil, err
	}

	things = append(things, *thing)

	return things, nil
}

// buildThing returns a thingfulx.thing based on the data provided
func (i *indexer) buildThing(sData BathingWaterFeed, dataURL string, timeProvider thingfulx.TimeProvider, rawData []byte) (*thingfulx.Thing, error) {

	webpage := sData.Result.Topic.StationLocationName.StationURI

	thing := thingfulx.Thing{
		Title:       fmt.Sprintf("%s bathing water quality", sData.Result.Topic.StationLocationName.Name.Value),
		Description: fmt.Sprintf("Bathing Water Monitoring Station: %s", sData.Result.Topic.SamplingPointLocation.SamplingName.Value),
		Category:    thingfulx.Environment,
		Webpage:     webpage,
		IndexedAt:   timeProvider.Now(),
		Location: &thingfulx.Location{
			Lat: sData.Result.Topic.SamplingPointLocation.Lat,
			Lng: sData.Result.Topic.SamplingPointLocation.Lgn,
		},
		Provider:   i.provider,
		Visibility: thingfulx.Open,
		Endpoint: &thingfulx.Endpoint{
			URL:         dataURL,
			ContentType: "application/json",
		},
		Metadata:        []thingfulx.Metadata{},
		ThingType:       schema.Expand("thingful:ConnectedDevice"),
		DataLicense:     thingfulx.GetDataLicense(dataLicenseURL), // need to check this
		AttributionName: attributionName,
		AttributionURL:  attributionURL,
	}

	// add channels
	channels, err := extractChannels(sData.Result.Topic, thing.Location)
	if err != nil {
		thing.Channels = []thingfulx.Channel{}
	} else {
		thing.Channels = channels
	}

	return &thing, nil
}

func extractChannels(stationData topic, loc *thingfulx.Location) ([]thingfulx.Channel, error) {

	// convert time
	// must add UTC offset suffix at the end of the string to allow parsing
	recordedAt, err := time.Parse(time.RFC3339, stationData.RecordedAt.Time.Value+"Z")
	if err != nil {
		return nil, err
	}

	ecDataType := schema.IntegerType
	ecValue := strconv.Itoa(stationData.ECCount)

	if stationData.ECQualifier.Notation != "=" {
		ecDataType = schema.StringType
		ecValue = stationData.ECQualifier.Notation + strconv.Itoa(stationData.ECCount)
	}

	ieDataType := schema.IntegerType
	ieValue := strconv.Itoa(stationData.IECount)

	if stationData.IEQualifier.Notation != "=" {
		ieDataType = schema.StringType
		ieValue = stationData.IEQualifier.Notation + strconv.Itoa(stationData.IECount)

	}

	channels := []thingfulx.Channel{
		{
			ID:           "escherichia_coli_count",
			Metadata:     []thingfulx.Metadata{},
			Type:         ecDataType,
			QuantityKind: schema.Expand("biotop:EscherichiaColiPopulation"),
			Unit:         schema.Expand("thingfulqu:ColoniesPer100MLWater"), //"colonies per 100ml water"
			DomainOfInterest: []string{
				schema.Expand("m3-lite:Environment"),
				schema.Expand("m3-lite:Health"),
			},
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        ecValue,
				},
			},
		},
		{
			ID:           "intestinal_enterococci_count",
			Metadata:     []thingfulx.Metadata{},
			Type:         ieDataType,
			QuantityKind: schema.Expand("thingfulqu:IntestinalEnterococciPopulation"),
			Unit:         schema.Expand("thingfulqu:ColoniesPer100MLWater"), //"colonies per 100ml water"
			DomainOfInterest: []string{
				schema.Expand("m3-lite:Environment"),
				schema.Expand("m3-lite:Health"),
			},
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        ieValue,
				},
			},
		},
	}

	return channels, nil
}
