package bathingwateruk

import (
	"context"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/thingful/httpmock"
	"github.com/thingful/thingfulx"
	"github.com/thingful/thingfulx/schema"
)

func mustReadFile(path string, t *testing.T) []byte {
	data, err := ioutil.ReadFile(path)
	assert.Nil(t, err)

	return data
}

func TestNewIndexer(t *testing.T) {
	_, err := NewIndexer()
	assert.Nil(t, err)
}

func TestURLS(t *testing.T) {
	indexer, _ := NewIndexer()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	delay := time.Duration(0)

	testcase := struct {
		inputFilePathEng   string
		inputFilePathWales string
		expected           []string
	}{
		inputFilePathEng:   "data/stations-data-eng.json",
		inputFilePathWales: "data/stations-data-wales.json",
		expected: []string{
			"http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			"http://environment.data.gov.uk/id/bathing-water/ukc2102-03700.json",
			"http://environment.data.gov.uk/id/bathing-water/ukc2102-03800.json",
			"http://environment.data.gov.uk/wales/bathing-waters/id/bathing-water/ukl2202-36100.json",
			"http://environment.data.gov.uk/wales/bathing-waters/id/bathing-water/ukl2202-36200.json",
		},
	}

	inputFileEngland, err := ioutil.ReadFile(testcase.inputFilePathEng)
	assert.Nil(t, err)

	inputFileWales, err := ioutil.ReadFile(testcase.inputFilePathWales)
	assert.Nil(t, err)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"http://environment.data.gov.uk/doc/bathing-water.json?_pageSize=1000",
			httpmock.NewBytesResponder(http.StatusOK, inputFileEngland),
		),
	)

	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"http://environment.data.gov.uk/wales/bathing-waters/doc/bathing-water.json?_pageSize=1000",
			httpmock.NewBytesResponder(http.StatusOK, inputFileWales),
		),
	)

	got, err := indexer.URLS(context.Background(), client, delay)
	assert.Nil(t, err)

	assert.Equal(t, testcase.expected, got)
}

func TestFetchValid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()
	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		stationRequestURL string
		sampleRequestURL  string
		stationResponse   []byte
		sampleResponse    []byte
		respStatusCode    int
	}{
		{
			stationRequestURL: "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			sampleRequestURL:  "http://environment.data.gov.uk/data/bathing-water-quality/in-season/sample/point/03600/date/20160919/time/100100/recordDate/20160919.json",
			stationResponse:   mustReadFile("data/valid-station.json", t),
			sampleResponse:    mustReadFile("data/valid-sample.json", t),
			respStatusCode:    http.StatusOK,
		},
		{
			stationRequestURL: "http://environment.data.gov.uk/wales/bathing-waters/id/bathing-water/ukl2202-36100.json",
			sampleRequestURL:  "http://environment.data.gov.uk/wales/bathing-waters/data/bathing-water-quality/in-season/sample/point/36100/date/20160912/time/142500/recordDate/20160912.json",
			stationResponse:   mustReadFile("data/valid-station2.json", t),
			sampleResponse:    mustReadFile("data/valid-sample2.json", t),
			respStatusCode:    http.StatusOK,
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.stationRequestURL,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.stationResponse),
			),
		)

		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.sampleRequestURL,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.sampleResponse),
			),
		)

		ctx := context.Background()

		bytes, err := indexer.Fetch(ctx, testcase.stationRequestURL, client)
		assert.Nil(t, err)

		assert.Equal(t, testcase.sampleResponse, bytes)
	}
}

func TestParseValid(t *testing.T) {

	indexer, _ := NewIndexer()

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())
	testRecorderAt, err := time.Parse(time.RFC3339, "2015-09-15T11:48:00Z")
	assert.Nil(t, err)

	test2RecorderAt, err := time.Parse(time.RFC3339, "2016-09-12T14:25:00Z")
	assert.Nil(t, err)

	testcases := []struct {
		stationRequestURL string
		sampleResponse    []byte
		expected          []thingfulx.Thing
	}{
		{
			stationRequestURL: "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			sampleResponse:    mustReadFile("data/valid-sample.json", t),
			expected: []thingfulx.Thing{
				thingfulx.Thing{
					Title:       "Spittal bathing water quality",
					Description: "Bathing Water Monitoring Station: Sampling point at Spittal",
					Category:    thingfulx.Environment,
					Webpage:     "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600",
					IndexedAt:   timeProvider.Now(),
					Location: &thingfulx.Location{
						Lat: 55.7568683458198,
						Lng: -1.9888393795735,
					},
					Provider: &thingfulx.Provider{
						Name:        provider,
						ID:          indexerUID,
						URL:         providerWebsite,
						Description: "The environment.data.gov.uk website provides open data from the Department of Environment, Food and Rural Affairs",
					},
					Visibility: thingfulx.Open,
					Endpoint: &thingfulx.Endpoint{
						URL:         "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
						ContentType: "application/json",
					},
					Metadata:        []thingfulx.Metadata{},
					ThingType:       schema.Expand("thingful:ConnectedDevice"),
					DataLicense:     thingfulx.GetDataLicense(thingfulx.OGLV3URL), // need to check this
					AttributionName: attributionName,
					AttributionURL:  attributionURL,
					Channels: []thingfulx.Channel{
						{
							ID:           "escherichia_coli_count",
							Metadata:     []thingfulx.Metadata{},
							Type:         schema.IntegerType,
							QuantityKind: schema.Expand("biotop:EscherichiaColiPopulation"),
							Unit:         schema.Expand("thingfulqu:ColoniesPer100MLWater"), //"colonies per 100ml water"
							DomainOfInterest: []string{
								schema.Expand("m3-lite:Environment"),
								schema.Expand("m3-lite:Health"),
							},
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecorderAt,
									Location: &thingfulx.Location{
										Lat: 55.7568683458198,
										Lng: -1.9888393795735,
									},
									Val: "270",
								},
							},
						},
						{
							ID:           "intestinal_enterococci_count",
							Metadata:     []thingfulx.Metadata{},
							Type:         schema.IntegerType,
							QuantityKind: schema.Expand("thingfulqu:IntestinalEnterococciPopulation"),
							Unit:         schema.Expand("thingfulqu:ColoniesPer100MLWater"), //"colonies per 100ml water"
							DomainOfInterest: []string{
								schema.Expand("m3-lite:Environment"),
								schema.Expand("m3-lite:Health"),
							},
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecorderAt,
									Location: &thingfulx.Location{
										Lat: 55.7568683458198,
										Lng: -1.9888393795735,
									},
									Val: "100",
								},
							},
						},
					},
				},
			},
		},
		{
			stationRequestURL: "http://environment.data.gov.uk/wales/bathing-waters/id/bathing-water/ukl2202-36100.json",
			sampleResponse:    mustReadFile("data/valid-sample2.json", t),
			expected: []thingfulx.Thing{
				thingfulx.Thing{
					Title:       "Jackson's Bay Barry Island bathing water quality",
					Description: "Bathing Water Monitoring Station: Sampling point at Jackson's Bay Barry Island",
					Category:    thingfulx.Environment,
					Webpage:     "http://environment.data.gov.uk/wales/bathing-waters/id/bathing-water/ukl2202-36100",
					IndexedAt:   timeProvider.Now(),
					Location: &thingfulx.Location{
						Lat: 51.3913601147912,
						Lng: -3.26328514212383,
					},
					Provider: &thingfulx.Provider{
						Name:        provider,
						ID:          indexerUID,
						URL:         providerWebsite,
						Description: "The environment.data.gov.uk website provides open data from the Department of Environment, Food and Rural Affairs",
					},
					Visibility: thingfulx.Open,
					Endpoint: &thingfulx.Endpoint{
						URL:         "http://environment.data.gov.uk/wales/bathing-waters/id/bathing-water/ukl2202-36100.json",
						ContentType: "application/json",
					},
					Metadata:        []thingfulx.Metadata{},
					ThingType:       schema.Expand("thingful:ConnectedDevice"),
					DataLicense:     thingfulx.GetDataLicense(thingfulx.OGLV3URL), // need to check this
					AttributionName: attributionName,
					AttributionURL:  attributionURL,

					Channels: []thingfulx.Channel{
						{
							ID:           "escherichia_coli_count",
							Metadata:     []thingfulx.Metadata{},
							Type:         schema.IntegerType,
							QuantityKind: schema.Expand("biotop:EscherichiaColiPopulation"),
							Unit:         schema.Expand("thingfulqu:ColoniesPer100MLWater"), //"colonies per 100ml water"
							DomainOfInterest: []string{
								schema.Expand("m3-lite:Environment"),
								schema.Expand("m3-lite:Health"),
							},
							Observations: []thingfulx.Observation{
								{
									RecordedAt: test2RecorderAt,
									Location: &thingfulx.Location{
										Lat: 51.3913601147912,
										Lng: -3.26328514212383,
									},
									Val: "45",
								},
							},
						},
						{
							ID:           "intestinal_enterococci_count",
							Metadata:     []thingfulx.Metadata{},
							Type:         schema.StringType,
							QuantityKind: schema.Expand("thingfulqu:IntestinalEnterococciPopulation"),
							Unit:         schema.Expand("thingfulqu:ColoniesPer100MLWater"), //"colonies per 100ml water"
							DomainOfInterest: []string{
								schema.Expand("m3-lite:Environment"),
								schema.Expand("m3-lite:Health"),
							},
							Observations: []thingfulx.Observation{
								{
									RecordedAt: test2RecorderAt,
									Location: &thingfulx.Location{
										Lat: 51.3913601147912,
										Lng: -3.26328514212383,
									},
									Val: "<10",
								},
							},
						},
					},
				},
			},
		},
	}

	for _, testcase := range testcases {

		things, err := indexer.Parse(testcase.sampleResponse, testcase.stationRequestURL, timeProvider)
		assert.Nil(t, err)

		assert.Equal(t, testcase.expected, things)
	}
}

func TestFetchInvalid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
	}{
		{
			url:            "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			respStatusCode: http.StatusNotFound,
			respBody:       mustReadFile("data/valid-station.json", t),
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		ctx := context.Background()

		_, err := indexer.Fetch(ctx, testcase.url, client)
		assert.NotNil(t, err)
	}
}

func TestParseInvalid(t *testing.T) {

	indexer, _ := NewIndexer()

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
	}{
		{
			url:            "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			respStatusCode: http.StatusOK,
			respBody:       mustReadFile("data/invalid-station.json", t),
		},
	}

	for _, testcase := range testcases {

		_, err := indexer.Parse(testcase.respBody, testcase.url, timeProvider)
		assert.NotNil(t, err)
	}
}

func TestFetchResponseError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		requestURL     string
		respStatusCode int
		expected       error
	}{
		{
			requestURL:     "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			respStatusCode: http.StatusNotFound,
			expected:       thingfulx.ErrNotFound,
		},
		{
			requestURL:     "http://environment.data.gov.uk/id/bathing-water/ukc2102-03600.json",
			respStatusCode: http.StatusRequestTimeout,
			expected:       thingfulx.NewErrUnexpectedResponse("408"),
		},
	}

	for _, testcase := range testcases {
		jsonResponse, err := ioutil.ReadFile("data/valid-station.json")
		assert.Nil(t, err)

		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.requestURL,
				httpmock.NewBytesResponder(testcase.respStatusCode, jsonResponse),
			),
		)

		ctx := context.Background()

		_, err = indexer.Fetch(ctx, testcase.requestURL, client)
		assert.NotNil(t, err)

		assert.Equal(t, testcase.expected, err)
	}
}

func TestFetchError(t *testing.T) {
	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []string{"", "%"}

	ctx := context.Background()

	for _, url := range testcases {
		_, err := indexer.Fetch(ctx, url, client)
		assert.NotNil(t, err)
	}
}
