// +build harness

package main

import (
	"bitbucket.org/thingful/bathingwateruk"
	"github.com/thingful/testharness"
	"golang.org/x/net/context"
	"time"
)

var URLs []string
var err error

func main() {

	harness, err := testharness.Register(bathingwateruk.NewIndexer, true)

	if err != nil {
		panic(err)
	}

	// run everything, Provider, URLs and Fetch

	fetchAllInterval := time.Duration(5) * time.Second // interval between each Fetch
	urlsToFetch := 10                                  // total of urls to Fetch
	harness.RunAll(context.Background(), fetchAllInterval, urlsToFetch)

	// or you can  fetch specific urls like this

	// urls := []string{
	// 	"api.example.com",
	// 	"api.example2.com",
	// }
	// fetchInterval := time.Duration(5) * time.Second // interval between each Fetch
	// harness.RunFetch(context.Background(), urls, fetchInterval)

}
