# Bathing Water UK

A Thingful fetcher for getting data from the environment.data.gov.uk bathing water monitoring API.
This API returns data about coastal and inland bathing water quality collected during the May–September in the UK and Wales.

## Type of fetch
This fetcher makes an initial get request to the top level API entrypoint and recursively works through all the paginated responses in order to extract individual monitoring station URLs.


The data returned to Thingful include stations latitude and longitude, the number of colonies of escherichia coli per 100ml water sample and the number of colonies of intestinal enterococci per 100ml water sample.
